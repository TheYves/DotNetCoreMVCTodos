﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MVCTodos.Interfaces;

namespace MVCTodos.Controllers
{
    public class TasksController : Controller
    {
        private ITaskRepository taskRepository;

        public TasksController(ITaskRepository repo)
        {
            taskRepository = repo;
        }

        public IActionResult Index()
        {
            var tasks = taskRepository.GetAllTasks().Result;
            return View(tasks);
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            var task = taskRepository.GetTask(id).Result;
            return View(task);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit([FromForm] Model.Task task, string id)
        {
            if (ModelState.IsValid)
            {
                task.Id = ObjectId.Parse(id);
                taskRepository.UpdateTask(task);
                return RedirectToAction("Index");
            }
            return View(task);
        }

        [HttpGet]
        public IActionResult Create()
        {
            var task = new Model.Task();
            return View(task);
        }

        [HttpGet]
        public IActionResult Delete(string id)
        {
            taskRepository.RemoveTask(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult ToggleDone(string id)
        {
            var task = taskRepository.GetTask(id).Result;
            task.Done = !task.Done;
            taskRepository.UpdateTask(task);
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([FromForm] Model.Task task)
        {
            if (ModelState.IsValid)
            {
                taskRepository.AddTask(task);
                return RedirectToAction("Index");
            }
            return View(task);
        }
    }
}
