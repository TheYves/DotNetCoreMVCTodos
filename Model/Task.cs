using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MVCTodos.Model
{
    public class Task
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public bool Done { get; set; } = false;
    }
}