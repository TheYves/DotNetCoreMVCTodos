using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MVCTodos.Model;

namespace MVCTodos.Data
{
    public class DbContext
    {
        private readonly IMongoDatabase database = null;

        public DbContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<Model.Task> Tasks
        {
            get
            {
                return database.GetCollection<Model.Task>("Task");
            }
        }
    }
}