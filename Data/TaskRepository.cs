using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using MVCTodos.Interfaces;
using MVCTodos.Model;

namespace MVCTodos.Data
{
    public class TaskRepository : ITaskRepository
    {
        private readonly DbContext context;

        public TaskRepository(IOptions<Settings> settings)
        {
            context = new DbContext(settings);
        }

        public async System.Threading.Tasks.Task AddTask(Model.Task item)
        {
            await context.Tasks.InsertOneAsync(item);
        }

        public async Task<IEnumerable<Model.Task>> GetAllTasks()
        {
            return await context.Tasks.Find(_ => true).ToListAsync();
        }

        public async Task<Model.Task> GetTask(string id)
        {
            return await context.Tasks.Find(t => t.Id == ObjectId.Parse(id)).FirstOrDefaultAsync();
        }

        public async Task<bool> RemoveTask(string id)
        {
            var result = await context.Tasks.DeleteOneAsync(t => t.Id == ObjectId.Parse(id));
            return result.IsAcknowledged && result.DeletedCount > 0;
        }

        public async Task<bool> UpdateTask(Model.Task task)
        {
            ReplaceOneResult result = await context.Tasks.ReplaceOneAsync(t => t.Id == task.Id, task);
            return result.IsAcknowledged && result.ModifiedCount > 0;
        }

    }
}