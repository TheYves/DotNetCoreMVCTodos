
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVCTodos.Interfaces 
{
    public interface ITaskRepository
    {
        Task<IEnumerable<Model.Task>> GetAllTasks();
        Task<Model.Task> GetTask(string id);

        Task AddTask(Model.Task item);

        Task<bool> RemoveTask(string id);

        Task<bool> UpdateTask(Model.Task task);
    }
}